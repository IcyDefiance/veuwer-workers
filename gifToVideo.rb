#!/usr/bin/ruby

require 'mysql2'
require 'aws-sdk'

db = Mysql2::Client.new(
    host: '*****',
    username: '*****',
    password: '*****',
    port: *****,
    database: '*****'
)

aws = Aws::S3::Client.new(
  region: '*****',
  credentials: Aws::Credentials.new('*****', '*****')
)

while true
  gifs = db.query("select id from images where mimetype = 'image/gif' and converted = false order by id")
  gifs.each do |row|
    id = row['id']

    aws.get_object(
      response_target: "tmp/#{id}.gif",
      bucket: 'veuwer',
      key: "images/#{id.to_s(36)}.png"
    )

    %x(ffmpeg -i tmp/#{id}.gif tmp/#{id}.mp4)
    %x(ffmpeg -i tmp/#{id}.gif tmp/#{id}.webm)

    File.open("tmp/#{id}.mp4") do |file|
      aws.put_object(
         bucket: 'veuwer',
         content_type: 'video/mp4',
         key: "videos/#{id.to_s(36)}.mp4",
         body: file
      )
    end

    File.open("tmp/#{id}.webm") do |file|
      aws.put_object(
        bucket: 'veuwer',
        content_type: 'video/webm',
        key: "videos/#{id.to_s(36)}.webm",
        body: file
      )
    end

    db.query("update images set converted = true where id = #{id}")

    %x(rm tmp/*)
  end
  sleep(10)
end
